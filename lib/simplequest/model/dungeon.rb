require 'json'
require 'simplequest/model/room'

class Dungeon

  attr_reader :rooms, :graph, :nodes

  def initialize(json)

    @rooms = {}
    @graph = {}
    @nodes = Array.new
    @INFINITY = 1 << 64

    file = File.read(json)
    rooms_struct = JSON.parse(file)
    build_from_struct(rooms_struct['rooms'])

    get_random_room.is_target_room = true

  end

  def get_room(symbol)
    @rooms[symbol]
  end

  def get_random_room
    @rooms.to_a.sample[1]
  end

  def get_a_room_far_from(source)
    last = get_paths(source).last
    return @rooms[last[rand(last.length-2..last.length-1)]]
  end

  def get_shortest_path(source, target)
    @source = source
    dijkstra source
    a = []
    path_to_a(a, @nodes[@nodes.index(target)])
  end

  def lock_if_bidirectional(destination, from, direction)

    room = get_room(destination)

    room.doors.each do |k, corridors|
      unless corridors.nil?
        corridors.each do |c|
          if c.target == from && c.through == direction
            # found one remove it
            corridors.delete c
          end
        end
      end
    end

  end

  private

  def build_from_struct(structs)

    structs.each do |r|
      room = Room.new(r)
      @rooms[room.name] = room
    end

    @rooms.each do |room_key, room|
      room.doors.each do |door_key, corridor_array|
        unless corridor_array.nil?
          corridor_array.each do |c|
            connect_room(c.source, c.target)
          end
        end
      end
    end

  end

  def connect_room(source, target)

    if (!@graph.has_key?(source))
      @graph[source] = {target => 1}
    else
      @graph[source][target] = 1
    end

    if (!@nodes.include?(source))
      @nodes << source
    end
    if (!@nodes.include?(target))
      @nodes << target
    end
    
  end

  def get_paths(source)
    @source = source
    dijkstra source
    paths = []
    @nodes.each do |dest|
      a = []
      path_to_a(a, dest)
      if a.length > 1
        paths.push(a)
      end
    end
    paths
  end

  def path_to_a(a, dest)
    if @prev[dest] != -1
      path_to_a(a, @prev[dest])
    end
    a.push(dest)
  end

  # from wikipedia's pseudocode: http://en.wikipedia.org/wiki/Dijkstra's_algorithm

  def dijkstra(s)
    @d = {}
    @prev = {}

    @nodes.each do |i|
      @d[i] = @INFINITY
      @prev[i] = -1
    end

    @d[s] = 0
    q = @nodes.compact
    while (q.size > 0)
      u = nil;
      q.each do |min|
        if (!u) or (@d[min] and @d[min] < @d[u])
          u = min
        end
      end
      if (@d[u] == @INFINITY)
        break
      end
      q = q - [u]
      unless @graph[u].nil?
        @graph[u].keys.each do |v|
          alt = @d[u] + @graph[u][v]
          if (alt < @d[v])
            @d[v] = alt
            @prev[v]  = u
          end
        end
      end
    end
  end

end