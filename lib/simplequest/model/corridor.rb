# corridor is an edge

class Corridor

  attr_accessor :source, :target, :through, :distance

  def initialize(vertexA, vertexB, through)
    self.source = vertexA
    self.target = vertexB
    self.through = through
    self.distance = 1
  end

end