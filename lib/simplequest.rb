require "highline/import"
require "simplequest/version"
require "simplequest/model/dungeon"
require "simplequest/model/hero"
require "simplequest/model/grue"
require "simplequest/controller/commands"
require 'simplequest/helpers/messages'

module Simplequest

  class Game

    include Commands

    def initialize
      super()
      $prod = true
    end

    def run

      begin

        startup File.join(File.dirname(__FILE__), "simplequest/support/dungeon.json")
        spawn_hero

        while !@hero.victory do

          # incrementing turn count is handled by commands
          if (@hero.turn_count > 0 && @hero.turn_count % 5 == 0)
            rest
          else
            show_choices
          end

          if @hero.ded
            ded
          end

        end

        victory

      rescue SystemExit, Interrupt
        puts "\n\nThanks for playing! bye!\n\n"
      end

    end

  end

end