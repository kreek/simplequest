Bundler.require(:default, :development)
require 'simplequest/model/dungeon'
require 'simplequest/model/hero'
require 'simplequest/model/room'

describe 'Dungeon' do

  before(:all) do
    @json_path = File.join(File.dirname(__FILE__), "support/dungeon.json")
    @dungeon = Dungeon.new(@json_path)
  end

  it 'should have multiple rooms' do
    @dungeon.rooms.length.should == 9
  end

  it 'should give a random room' do
    @dungeon.get_random_room.is_a? Room
  end

  it 'should return a room that is far from another' do
    ([:lavender, :emerald].include? @dungeon.get_a_room_far_from(:vermillion).name).should == true
  end

  it 'should get the shortest path between two rooms' do
    @dungeon.get_shortest_path(:vermillion, :lavender).should == [:vermillion, :aquamarine, :emerald, :lavender]
    @dungeon.get_shortest_path(:chartreuse, :emerald).should == [:chartreuse, :emerald]
    @dungeon.get_shortest_path(:burnt_sienna, :vermillion).should == [:burnt_sienna, :emerald, :cobalt, :vermillion]
  end

  it 'should lock if it is bidirectional' do

    # reset dungeon
    @dungeon = Dungeon.new(@json_path)
    # move hero to emerald
    @hero = Hero.new(@dungeon)
    @hero.spawn
    @hero.move_to @dungeon.get_room(:emerald)

    # get a destination (random will be aquamarine or burnt_sienna)
    destination = @hero.current_room.destination(:south)

    @dungeon.lock_if_bidirectional(destination, @hero.current_room.name, :south)

    if destination == :aquamarine
      @dungeon.get_room(:aquamarine).door_locked?(:east).should == true
    else
      @dungeon.get_room(:burnt_sienna).door_locked?(:north).should == true
    end

  end

  it 'should have a target room' do
    @dungeon.rooms.any? {|k, v| v.is_target_room}.should == true
  end

end