class Grue < Character

  def initialize(dungeon)
    super(dungeon)
  end

  def hero_spawned(location)
    room = @dungeon.get_a_room_far_from(location)
    self.move_to(room)
  end

  def hero_moved_to(location)
    @just_fled = false
    if (location == self.location)
      self.flee
    end
  end

  def hero_is_resting(location)
    if (location != self.location)
      path = @dungeon.get_shortest_path(self.location, location)
      self.move_to( @dungeon.get_room(path[1]) ) unless @just_fled
      # kill?
      if (location == self.location)
        dispatch_event(:hero_attacked, 1)
      end
    end
  end

  def flee
    @just_fled = true
    @available_rooms = @dungeon.get_room(self.location).doors.reject {|k,v| v.nil?}
    target = @available_rooms.to_a.sample[1].sample.target
    self.move_to(@dungeon.get_room(target))
    dispatch_event(:grue_dropped_gem, 1)
  end

end