class Messages

  def self.logo

    return unless $prod

    lines = [
        " _____ _           _",
        "|   __|_|_____ ___| |___",
        "|__   | |     | . | | -_|",
        "|_____|_|_|_|_|  _|_|___|",
        "  |     |_ _ _|_|___| |_",
        "  |  |  | | | -_|_ -|  _|",
        "  |__  _|___|___|___|_|",
        "     |__|",
        "  ",
        "",
        " \u00A9 2013 Alastair Dawson"
    ]

    lines.each do |line|
      say("#{HighLine.new.color(line, HighLine::RED)}")
      sleep(0.2)
    end

    say("\n\n")

  end

  def self.startup

    return unless $prod

    logo

    say("YOU HAVE WOKEN UP IN A STRANGE DUNGEON.")

    say("\nThere's a note scratched into the floor in front of you. It reads:")
    say("\n    #{HighLine.new.color("To escape the Grue's hive startle him for five.", HighLine::RED)}")
    say("    #{HighLine.new.color("It's been a while since he fed; if he finds you yer ded.", HighLine::RED)}")

  end

  def self.door_locked

    return unless $prod

    puts "\e[H\e[2J"

    say("You hear a voice shout:")
    message = ["    The way is shut!", "    You shall not pass!", "    What is the airspeed velocity of an unladen swallow?", "    It's a trap! (not really you can go another direction)"]
    say("\n#{HighLine.new.color(message.sample, HighLine::RED)}")
    say("\nLooks like that door is locked, try another direction...")
    say("\n")

  end

  def self.door_open

    return unless $prod

    puts "\e[H\e[2J"

    say("You walk down a corridor and...")

  end

  def self.arrived_room(location)
    return unless $prod
    say("\nThe room you're now in is #{HighLine.new.color(location, HighLine::UNDERLINE)} in color; it has four doors.")
  end


  def self.rest

    return unless $prod

    say("\n    Man, that's a lot of walking!!! Should have picked a profession where you don't sit so much...")
    say("\n        Have a beer while you rest #SleepyHour")

    ask("\n            Mash your keyboard to continue...") {}

  end

  def self.grue_fled(gems)
    return unless $prod
    say("\n    What was that! Some strange creature just ran out of the room!")
    say("\n        #{HighLine.new.color("Looks like he dropped a gem; you now have #{gems}.", HighLine::GREEN)}")
  end

  def self.ded

    return unless $prod

    puts "\e[H\e[2J"

    say("\nWait... what's thaAAAAAAaaa #{HighLine.new.color("AAAaAaaa (oh the horror!) ", HighLine::RED)} aaa... #{HighLine.new.color("AAA!!! AAAAAA!!! AAAAaaa aaa... a...  a...", HighLine::RED)}")
    say("\n    #{HighLine.new.color("Yer ded.", HighLine::RED)}")
    say("\n        You might have died but there is no escape! GRUEHOG DAY CONTINUES...")
    ask("\n            Mash your keyboard to continue...") {}
  end

  def self.target_room
    return unless $prod
    say("\n    #{HighLine.new.color("You notice that this room has 5 pedestals for gems", HighLine::GREEN)}")
  end

  def self.spawn
    return unless $prod
    say("\n")
  end

  def self.victory?(gems)
    return unless $prod
    say("\n    #{HighLine.new.color("You have #{gems}! you carefully place a gem on each pedestal...", HighLine::GREEN)}")
    say("    #{HighLine.new.color("Suddenly a blinding light fills the room...", HighLine::GREEN)}")
    ask("\n            Mash your keyboard to continue...") {}
    say("\nYOU HAVE WOKEN UP IN A STRANGE DUNGEON. THERE'S A CORPSE ON THE FLOOR.")
    say("\nNext to it there's a note scratched in the dirt. It reads:")
    say("\n    #{HighLine.new.color("You beat my Grue now you are one too.", HighLine::RED)}")
    say("    #{HighLine.new.color("Another human has passed through my gates. Kill them if you want to escape.", HighLine::RED)}")

  end

end