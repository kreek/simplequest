module EventDispatcher

  def initialize
    @listeners={}
  end

  # Maps a symbol to a target's method of the same name
  def add_listener(event, target)
    @listeners[event] = Proc.new {|payload| target.send(event, payload) }
  end

  def remove_listener(event)
    @listeners.delete(event)
  end

  # Sends a message to run target method, with optional payload (args)
  def dispatch_event(event, payload = {})
    begin
      @listeners[event].call(payload)
    rescue NoMethodError
      puts "NoMethodError: Doh! event: #{event} is not bound to a listener" unless !$prod
    end
  end

end