Bundler.require(:default, :development)
require 'simplequest/model/hero'
require 'simplequest/model/dungeon'

describe 'Hero' do

  before(:all) do
    dungeon_json = File.join(File.dirname(__FILE__), "support/dungeon.json")
    @dungeon = Dungeon.new(dungeon_json)
    @hero = Hero.new(@dungeon)
    @hero.spawn
  end

  it 'should have a location' do
    @hero.location.nil?.should_not == true
  end

  it 'should win' do
    @hero.gems = 5
    target_room = @dungeon.rooms.select {|k, v| v.is_target_room }.values[0]
    @hero.move_to(target_room)
    @hero.victory.should == true
  end

end