require 'simplequest/model/character'
require 'simplequest/helpers/messages'

class Hero < Character

  attr_accessor :gems,
                :ded,
                :turn_count,
                :victory

  def initialize(dungeon)
    super(dungeon)
  end

  def spawn
    self.turn_count = 1
    self.victory = false
    self.ded = false
    self.gems = 0
    move_to(@dungeon.get_random_room, true)
    #move_to(@dungeon.get_room(:burnt_sienna), true)
    dispatch_event(:hero_spawned, self.location)
  end

  def move_to(room, init=false)
    super(room)
    Messages.arrived_room self.location.to_s
    dispatch_event(:hero_moved_to, self.location) unless init
    check_room
  end

  def rest
    unless self.ded
      Messages.rest
      dispatch_event(:hero_is_resting, self.location)
    else
      Messages.spawn
      self.spawn
    end
  end

  def grue_dropped_gem(gem)
    self.gems += gem
    Messages.grue_fled self.gems
  end

  def hero_attacked(body)
    self.ded = true
  end

  private

  def check_room
    if self.current_room.is_target_room
      Messages.target_room
      if self.gems >= 5
        self.victory = true
      end
    end
  end

end