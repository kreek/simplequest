Bundler.require(:default, :development)
require 'simplequest/model/hero'
require 'simplequest/model/grue'


describe 'Grue' do

  before(:all) do
    # load json
    dungeon_json = File.join(File.dirname(__FILE__), "support/dungeon.json")
    @dungeon = Dungeon.new(dungeon_json)
    # create characters
    @hero = Hero.new(@dungeon)
    @grue = Grue.new(@dungeon)
    # set up listerners
    @hero.add_listener(:hero_spawned, @grue)
    @hero.add_listener(:hero_moved_to, @grue)
    @hero.add_listener(:hero_is_resting, @grue)
    @grue.add_listener(:grue_dropped_gem, @hero)
    @grue.add_listener(:hero_attacked, @hero)
    # go!
    @hero.spawn
  end

  it 'should move one room closer when the hero rests' do
    @hero.move_to(@dungeon.rooms[:vermillion])
    @grue.move_to(@dungeon.rooms[:aquamarine])
    @hero.rest
    @grue.location.should == :cobalt
  end

  it 'should drop a gem when it flees' do
    @grue.flee
    @hero.gems.should == 1
  end

  it 'should flee to an adjacent room' do
    @grue.move_to(@dungeon.rooms[:emerald])
    @grue.flee
    ([:burnt_sienna, :lavender, :cobalt, :aquamarine].include? @grue.location).should == true
    (@grue.location == :chartreuse).should == false
  end

  it 'should kill the hero' do
    @hero.move_to(@dungeon.rooms[:vermillion])
    @grue.move_to(@dungeon.rooms[:ochre])
    @hero.rest
    @hero.ded.should == true
  end

end