# Simplequest

Simplequest game for Sleepy Giant

## Installation

clone this repo then:

    $ cd simplequest
	$ bundle install
	$ gem build simplequest.gemspec

Run tests:

    $ bundle exec rspec --color --format d

Install and play:

    $ bundle exec rake install
	$ bundle exec simplequest
