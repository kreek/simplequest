require 'simplequest/model/event_dispatcher'

class Character

  include EventDispatcher
  attr_accessor :current_room

  def initialize(dungeon)
    super()
    @dungeon = dungeon
  end

  def move_to(room)
    self.current_room = room
  end

  def location
    self.current_room.name
  end

end