# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'simplequest/version'

Gem::Specification.new do |spec|
  spec.name          = "simplequest"
  spec.version       = Simplequest::VERSION
  spec.authors       = ["Alastair Dawson"]
  spec.email         = ["alastair.j.dawson@gmail.com"]
  spec.description   = "Sleepy Giant's coding challenge game Simple Quest."
  spec.summary       = "Simple Quest is a turn based text game in which a player tries to escape a dungeon."
  spec.homepage      = "http://sleepygiant.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = ["simplequest"]
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 2.6"
  spec.add_runtime_dependency "highline", "~> 1.6"

end
