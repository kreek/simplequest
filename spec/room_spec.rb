Bundler.require(:default, :development)
require 'simplequest/model/room'

describe 'Room' do

  before(:all) do
    struct = {"name"=>"burnt_sienna", "north"=>[{"to_room"=>"emerald", "through"=>"south"}], "east"=>[{"to_room"=>"lavender", "through"=>"south"}], "south"=>nil, "west"=>[]}
    @room = Room.new(struct)
  end

  it 'should create itself from struct' do
    @room.name.should == :burnt_sienna
  end

  it 'should let the player try a door' do
    @room.door_locked?(:north).should == false
    @room.door_locked?(:south).should == true
    @room.door_locked?(:west).should == true
  end

end