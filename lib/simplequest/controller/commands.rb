require "simplequest/helpers/messages"

module Commands

  def startup(dungeon_json)

    puts "\e[H\e[2J" # clear the console

    # create map
    @dungeon_json = dungeon_json
    @dungeon = Dungeon.new(@dungeon_json)
    # create characters
    @hero = Hero.new(@dungeon)
    @grue = Grue.new(@dungeon)
    # set up listerners
    @hero.add_listener(:hero_spawned, @grue)
    @hero.add_listener(:hero_moved_to, @grue)
    @hero.add_listener(:hero_is_resting, @grue)
    @grue.add_listener(:grue_dropped_gem, @hero)
    @grue.add_listener(:hero_attacked, @hero)

  end

  def spawn_hero
    # go!
    Messages.startup
    @hero.spawn
    @hero.turn_count = 1
  end

  def show_choices

    choose do |menu|

      menu.layout = :one_line
      menu.prompt = "\nWhich way would you like to go?  "

      menu.choice "north" do
        move :north
      end

      menu.choice "east" do
        move :east
      end

      menu.choice "south" do
        move :south
      end

      menu.choice "west" do
        move :west
      end

    end

  end

  def move(direction)

    if @hero.current_room.door_locked? direction
      puts @hero.current_room.door_locked? direction
      puts @hero.current_room.inspect
      Messages.door_locked
    else
      Messages.door_open
      destination = @hero.current_room.destination(direction)
      room = @dungeon.get_room(destination)
      @dungeon.lock_if_bidirectional(destination, @hero.current_room.name, direction)
      @hero.move_to room
      @hero.turn_count += 1
    end

  end

  def rest
    @hero.turn_count += 1
    @hero.rest
  end

  def ded
    Messages.ded
    startup @dungeon_json
    spawn_hero
  end

  def victory
    Messages.victory? @hero.gems
    exit
  end

end