# room is a node
require 'simplequest/model/corridor'

class Room

  attr_accessor :name,
                :doors,
                :is_target_room

  def initialize(struct)
    self.name = struct['name'].to_sym
    self.doors = {}
    self.doors[:north] = corridors_from_struct(struct['north'])
    self.doors[:east] = corridors_from_struct(struct['east'])
    self.doors[:south] = corridors_from_struct(struct['south'])
    self.doors[:west] = corridors_from_struct(struct['west'])
  end

  def door_locked?(symbol)
    self.doors[symbol].nil? || self.doors[symbol].length == 0
  end

  def destination(direction)
    self.doors[direction].sample.target
  end

  private

  def corridors_from_struct(struct)

    corridor_array = nil

    unless struct.nil?
      corridor_array = struct.map {|s|
        Corridor.new(self.name, s["to_room"].to_sym, s["through"].to_sym)
      }
    end

    corridor_array

  end

end